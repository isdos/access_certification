package com.example.isdos.assistdis

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_objects.*

class ObjectsActivity : AppCompatActivity(), ObjectFragment.OnListFragmentInteractionListener {

    override fun onListFragmentInteraction(item: Object?) {
        val intent = Intent(applicationContext, ObjectDetails::class.java)
        intent.putExtra("Place", item)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_objects)
        setSupportActionBar(toolbar)


        val objects = intent.getParcelableArrayListExtra<Place>("ObjectsActivity")
        val objectTypes  = intent.getStringArrayExtra("ObjTypes")

        val bundle = Bundle()
        bundle.putStringArray("objTypes", objectTypes)
        bundle.putParcelableArrayList("objects", objects)

        val fragment = ObjectFragment()
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().run {
            replace(R.id.sample_content_fragment, fragment)
            commit()
        }
    }
}

