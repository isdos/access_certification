package com.example.isdos.assistdis

import android.os.Parcel
import android.os.Parcelable

data class BfElements(
        private val id_elements: Int,
        val emergency_evacuation: Int,
        val parking: Int,
        val routes: Int,
        val entrance: Int,
        val access_blind: Int,
        val access_deaf: Int,
        val info_signs: Int,
        val trained_staff: Int,
        val lifts_stairs: Int
):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id_elements)
        parcel.writeInt(emergency_evacuation)
        parcel.writeInt(parking)
        parcel.writeInt(routes)
        parcel.writeInt(entrance)
        parcel.writeInt(access_blind)
        parcel.writeInt(access_deaf)
        parcel.writeInt(info_signs)
        parcel.writeInt(trained_staff)
        parcel.writeInt(lifts_stairs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BfElements> {
        override fun createFromParcel(parcel: Parcel): BfElements {
            return BfElements(parcel)
        }

        override fun newArray(size: Int): Array<BfElements?> {
            return arrayOfNulls(size)
        }
    }
}