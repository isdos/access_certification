package com.example.isdos.assistdis

import android.os.Parcel
import android.os.Parcelable

data class Place(private val id: Int,
            val name: String,
            val city: String,
            val address: String,
            val date: String,
            val coordinates: String,
            val mental_issues: Int,
            val visual_impairment: Int,
            val deafness: Int,
            val physical_disability: Int,
            val comment: String,
            val issue_mark: Int,
            val id_elem: Int,
            val reported: Int,
            val checked: Int,
            val place_type: Int,
            val id_elements: Int,
            val emergency_evacuation: Int,
            val parking: Int,
            val routes: Int,
            val entrance: Int,
            val access_blind: Int,
            val access_deaf: Int,
            val info_signs: Int,
            val trained_staff: Int,
            val lifts_stairs: Int): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    fun getId (): Int {
        return this.id
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(city)
        parcel.writeString(address)
        parcel.writeString(date)
        parcel.writeString(coordinates)
        parcel.writeInt(mental_issues)
        parcel.writeInt(visual_impairment)
        parcel.writeInt(deafness)
        parcel.writeInt(physical_disability)
        parcel.writeString(comment)
        parcel.writeInt(issue_mark)
        parcel.writeInt(id_elem)
        parcel.writeInt(reported)
        parcel.writeInt(checked)
        parcel.writeInt(place_type)
        parcel.writeInt(id_elements)
        parcel.writeInt(emergency_evacuation)
        parcel.writeInt(parking)
        parcel.writeInt(routes)
        parcel.writeInt(entrance)
        parcel.writeInt(access_blind)
        parcel.writeInt(access_deaf)
        parcel.writeInt(info_signs)
        parcel.writeInt(trained_staff)
        parcel.writeInt(lifts_stairs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Place> {
        override fun createFromParcel(parcel: Parcel): Place {
            return Place(parcel)
        }

        override fun newArray(size: Int): Array<Place?> {
            return arrayOfNulls(size)
        }
    }
}