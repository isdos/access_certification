package com.example.isdos.assistdis

import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TabItem
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener

import kotlinx.android.synthetic.main.activity_charts.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.content_charts.view.*
import kotlinx.android.synthetic.main.fragment_object.*
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter


class ChartsActivity : AppCompatActivity() {

    private lateinit var obj: ArrayList<Object>
    private lateinit var objTypes: Array<String>
    private lateinit var mPieChart: PieChart
    private lateinit var mBarChart: BarChart

    private val colors = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_charts)
        setSupportActionBar(toolbar)

        getColors()

        obj = intent.getParcelableArrayListExtra("obj")
        objTypes = intent.getStringArrayExtra("objTypes")

        val layout = findViewById<ConstraintLayout>(R.id.test)

        layout.setBackgroundColor(Color.parseColor("#55656C"))

        layout.findViewById<TabLayout>(R.id.tabs).apply {
            addTab(this.newTab().setText("Place type"), true)
            addTab(this.newTab().setText("MeanAccess"))
            addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    when (tab.position) {
                        0 -> mPieChart.visibility = View.VISIBLE
                        1 -> mBarChart.visibility = View.VISIBLE
                    }
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    when (tab.position) {
                        0 -> mPieChart.visibility = View.GONE
                        1 -> mBarChart.visibility = View.GONE
                    }
                }
            })
        }
        getPieChart()
        getBarChart()
    }

    private fun getColors() {

        for (c in ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
    }

    private fun getPieChart() {
        mPieChart = findViewById(R.id.pie_chart)
        mPieChart.isDrawHoleEnabled = true
        mPieChart.holeRadius = 7F
        mPieChart.transparentCircleRadius = 10F

        mPieChart.rotationAngle = 0F
        mPieChart.isRotationEnabled = true

        mPieChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {

            }

            override fun onNothingSelected() {

            }
        })
        addData()
    }

    private fun getBarChart() {
        mBarChart = findViewById(R.id.bar_chart)
        val dataXY = ArrayList<BarEntry>()
        val strins = arrayListOf("MeanVisual", "MeanMent", "MeanDeaf", "MeanPhys")
        val dataY = genDataAverage(obj)

        for (i in dataY.indices)
            dataXY.add(BarEntry(i.toFloat(),dataY[i]))

        val dataSet = BarDataSet(dataXY, "Mean values of object accessibility")
        dataSet.colors = colors

        val data = BarData(dataSet)
        mBarChart.apply {
            this.data = data
            this.data.barWidth = 1F
            animateXY(2000, 2000)
            legend.run {
                xEntrySpace = 7F
                yEntrySpace = 5F
            }
            xAxis.apply {
                isGranularityEnabled = true
                labelCount = dataY.size
            }
            invalidate()
        }

        mBarChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry, h: Highlight?) {
                Toast.makeText(applicationContext, "${strins[e.x.toInt()]}: ${e.y}", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected() {

            }
        })
    }

    private fun genDataAverage(obj: ArrayList<Object>): FloatArray {
        var sumVis = 0
        var sumMent = 0
        var sumDeaf = 0
        var sumPhys = 0
        val size = obj.size.toFloat()

        obj.forEach { sumVis += it.visual_impairment }
        obj.forEach { sumMent += it.mental_issues }
        obj.forEach { sumDeaf += it.deafness }
        obj.forEach { sumPhys += it.physical_disability }

        return floatArrayOf((sumVis/size), sumMent/size, sumDeaf/size, sumPhys/size)
    }

    private fun genDataPlaceType(obj: ArrayList<Object>, objTypes: Array<String>): Map<String, Float> {

        val map = HashMap<String, Float>()

        for (i in objTypes.indices)
            map[objTypes[i]] = obj.filter { it.place_type == i }.size.toFloat()

        return map.filter { it.value > 0 }
    }

    private fun addData() {
        val values = genDataPlaceType(obj, objTypes)

        val dataXY = ArrayList<PieEntry>()

        for ((k, v) in values)
            dataXY.add(PieEntry(v, k))

        val dataSet = PieDataSet(dataXY, "Amount of objects of some type")
        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f

        // add many colors

        dataSet.colors = colors

        // instantiate pie data object now
        val data = PieData(dataSet)

        data.setValueTextSize(11f)
        data.setValueTextColor(Color.GRAY)

        mPieChart.data = data

        mPieChart.highlightValues(null)

        mPieChart.legend.run {
            position = Legend.LegendPosition.BELOW_CHART_LEFT
            xEntrySpace = 7F
            yEntrySpace = 5F

        }

        mPieChart.invalidate()
    }

}
