package com.example.isdos.assistdis


class Places {
    private var places: ArrayList<Place> = ArrayList()

    fun getPlaces(): ArrayList<Place> {
        return places
    }

    fun setPlaces(places: ArrayList<Place>) {
        this.places = places
    }
}