package com.example.isdos.assistdis

import android.os.Parcel
import android.os.Parcelable


data class Object(
        private val id: Int,
        val name: String,
        val city: String,
        val address: String,
        val date: String,
        val coordinates: String,
        val mental_issues: Int,
        val visual_impairment: Int,
        val deafness: Int,
        val physical_disability: Int,
        val comment: String,
        val issue_mark: Int,
        private val id_elem: Int,
        val reported: Int,
        val checked: Int,
        val place_type: Int,
        val bf_elements: BfElements
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readParcelable(BfElements::class.java.classLoader)) {
    }

    val getId: Int
        get() = this.id

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(city)
        parcel.writeString(address)
        parcel.writeString(date)
        parcel.writeString(coordinates)
        parcel.writeInt(mental_issues)
        parcel.writeInt(visual_impairment)
        parcel.writeInt(deafness)
        parcel.writeInt(physical_disability)
        parcel.writeString(comment)
        parcel.writeInt(issue_mark)
        parcel.writeInt(id_elem)
        parcel.writeInt(reported)
        parcel.writeInt(checked)
        parcel.writeInt(place_type)
        parcel.writeParcelable(bf_elements, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Object(id=$id, name='$name', city='$city', address='$address', date='$date', coordinates='$coordinates', mental_issues=$mental_issues, visual_impairment=$visual_impairment, deafness=$deafness, physical_disability=$physical_disability, comment='$comment', issue_mark=$issue_mark, id_elem=$id_elem, reported=$reported, checked=$checked, place_type=$place_type, bf_elements=$bf_elements)"
    }

    companion object CREATOR : Parcelable.Creator<Object> {
        override fun createFromParcel(parcel: Parcel): Object {
            return Object(parcel)
        }

        override fun newArray(size: Int): Array<Object?> {
            return arrayOfNulls(size)
        }
    }
}