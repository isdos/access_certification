package com.example.isdos.assistdis

import com.google.gson.annotations.SerializedName

class Result(@field:SerializedName("error")
             private val error: Boolean?, @field:SerializedName("message")
             val message: String, @field:SerializedName("place")
             val place: Place) {

    fun getError(): Boolean {
        return error!!
    }
}
