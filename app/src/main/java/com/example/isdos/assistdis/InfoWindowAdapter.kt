package com.example.isdos.assistdis

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import android.widget.TextView



class InfoWindowAdapter : GoogleMap.InfoWindowAdapter {
    
    private val mContext: Context
    private val mWindow: View

    constructor(context: Context) {
        mContext = context
        mWindow = LayoutInflater.from(mContext).inflate(R.layout.info_window_adapter, null)
    }
    
    private fun rendWindowText(marker: Marker, view: View) {
        val title = marker.title
        val tvTitle = view.findViewById<TextView>(R.id.titleInfoWindow)

        if (title != "") {
            tvTitle.text = title
        }

        val snippet = marker.snippet
        val tvSnippet = view.findViewById<TextView>(R.id.snippetInfoWindow)

        if (snippet != "") {
            tvSnippet.text = snippet
        }
    }

    override fun getInfoContents(p0: Marker?): View {
        rendWindowText(p0!!, mWindow)
        return mWindow
    }

    override fun getInfoWindow(p0: Marker?): View {
        rendWindowText(p0!!, mWindow)
        return mWindow
    }
}