package com.example.isdos.assistdis

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.SeekBar.OnSeekBarChangeListener
import com.example.isdos.assistdis.R.id.*
import kotlinx.android.synthetic.main.activity_new_place.*
import kotlinx.android.synthetic.main.content_new_place.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class NewPlaceActivity : AppCompatActivity() {

    private var objPos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_place)
        setSupportActionBar(toolbar)
        getAddress()
        sendButton.setOnClickListener({ this.createPlace() })
    }

    private fun bfElements(switch: Switch): Int {
        return when (switch.isChecked) {
            true -> 1
            false -> 0
        }
    }

    private fun getObjType(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        objPos = position
    }

    private fun getAddress() {

        val coordinates = intent.getStringExtra("LatLong")
        if (coordinates.isNullOrEmpty())
            return
        val coords = coordinates!!.substring(10, coordinates.indexOf(')')).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val lat = coords[0].toDouble()
        val long = coords[1].toDouble()
        Geocoder(this, Locale.ENGLISH).getFromLocation(lat, long, 1)[0].run {
            val address = getAddressLine(0).split(",")
            ET_address.setText("${address[0]},${address[1]}")
            ET_city.setText(locality)
        }

    }
    private fun createPlace() {

        var name = ET_name.text.toString()
        var city = ET_city.text.toString()
        var address = ET_address.text.toString()
        var objType = objTypeSpinner.selectedItemPosition
        var comment = commentET.text.toString()
        var coordinates = intent.getStringExtra("LatLong")
        var emergEva = bfElements(emergencyEvacuationSwitch)
        var parking = bfElements(parkingSwitch)
        var routeToObj = bfElements(routesObjSwitch)
        var entrance = bfElements(entranceSwitch)
        var accBlind = bfElements(accessBlindSwitch)
        var accDeaf = bfElements(accessDeafSwitch)
        var infSgn = bfElements(infoSignSwitch)
        var trStaff = bfElements(trainedStaffSwitch)
        var lftStairs = bfElements(liftsStairsSwitch)
        var hearingRateValue: Int = hearing.rating.toInt()
        var physicalRateValue: Int = physical.rating.toInt()
        var visualRateValue: Int = visual.rating.toInt()
        var mentalRateValue: Int = mental.rating.toInt()

        val service = API.create()
        val call = service.newPlace(name, city, address, coordinates, mentalRateValue, visualRateValue, hearingRateValue, physicalRateValue, comment, issue_mark = 0, checked = 0, place_type = objType, emergency_evacuation = emergEva, parking = parking, routes_to_obj = routeToObj, entrance = entrance, access_blind = accBlind, access_deaf = accDeaf, info_signs = infSgn, trained_staff = trStaff, lifts_stairs = lftStairs, reported = 0)
        call.enqueue(object : Callback<Result> {
            override fun onResponse(call: Call<Result>, response: Response<Result>) {
                Toast.makeText(applicationContext, response.body()?.message, Toast.LENGTH_LONG).show()
                if (response.body()?.message == "Success") {
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure(call: Call<Result>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }
}
