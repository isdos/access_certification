package com.example.isdos.assistdis


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*
import kotlin.collections.ArrayList
import retrofit2.http.GET




interface API {

    @get:GET("places")
    val allPlaces: Call<ArrayList<Object>>

    @GET("filterPlaces")
    fun getFilteredPlace(
            @Query("city") city: String,
            @Query("place_type") place_type: String,
            @Query("bf_elements") bf_elements: String,
            @Query("val") value: String
    ): Call<ArrayList<Object>>

    @GET("filterPlaces")
    fun getFilteredPlaces(
            @Query("city") city: String,
            @Query("plTypes") plTypes: IntArray
    ): Call<ArrayList<Place>>

    @FormUrlEncoded
    @POST("createPlace")
    fun newPlace(
            @Field("name") name: String,
            @Field("city") city: String,
            @Field("address") address: String,
            @Field("coordinates") coordinates: String,
            @Field("mental_issues") mental_issues: Int,
            @Field("visual_impairment") visual_impairment: Int,
            @Field("deafness") deafness: Int,
            @Field("physical_disability") physical_disability: Int,
            @Field("comment") comment: String,
            @Field("issue_mark") issue_mark: Int,
            @Field("reported") reported: Int,
            @Field("checked") checked: Int,
            @Field("place_type") place_type: Int,
            @Field("emergency_evacuation") emergency_evacuation: Int,
            @Field("parking") parking: Int,
            @Field("routes_to_obj") routes_to_obj: Int,
            @Field("entrance") entrance: Int,
            @Field("access_blind") access_blind: Int,
            @Field("access_deaf") access_deaf: Int,
            @Field("info_signs") info_signs: Int,
            @Field("trained_staff") trained_staff: Int,
            @Field("lifts_stairs") lifts_stairs: Int
    ): Call<Result>

    companion object {
        fun create(): API {

            val retrofit = Retrofit.Builder()
                    .baseUrl(APIUrl.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(API::class.java)
        }
    }
}

