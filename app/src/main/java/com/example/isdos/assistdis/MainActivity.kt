package com.example.isdos.assistdis

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.TintableBackgroundView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.Toast
import android.widget.ProgressBar
import android.widget.SearchView
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.PlaceTypes
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener
import com.google.android.gms.maps.model.*
import com.miguelcatalan.materialsearchview.MaterialSearchView

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private lateinit var objectTypes: Array<String>
    private lateinit var bfElements: Array<String>
    private lateinit var bfElementsDb: Array<String>
    private lateinit var viewPoint: Array<String>
    private lateinit var checkedItems: BooleanArray
    private lateinit var checkedBfItems: BooleanArray
    private lateinit var flag: BooleanArray
    private lateinit var navCity: MenuItem
    private lateinit var objCity: ArrayList<Object>
    private lateinit var v2: ViewParent

    private var allPlaces = ArrayList<Object>()

    private var mMap: GoogleMap? = null
    private var marker: Marker? = null
    private var selectedViewPoint: Int = 0
    private val PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
    private var city = "Ufa"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        objectTypes = resources.getStringArray(R.array.obj_types)
        bfElements = resources.getStringArray(R.array.bf_elements)
        bfElementsDb = resources.getStringArray(R.array.bf_elements_db)
        viewPoint = resources.getStringArray(R.array.view_point)
        checkedItems = BooleanArray(objectTypes.size)
        checkedBfItems = BooleanArray(bfElements.size)

        navCity = findViewById<NavigationView>(R.id.nav_view).menu.findItem(R.id.nav_city)
        navCity.title = "${resources.getString(R.string.nav_city_button_menu)}: $city"

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val search = fragmentManager.findFragmentById(R.id.place_autocomplete) as PlaceAutocompleteFragment
        search.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place?) {
                if (marker != null)
                    marker!!.remove()
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(p0?.latLng, 20f))
                marker = mMap!!.addMarker(MarkerOptions().position(p0?.latLng!!).title(p0.name.toString()).snippet(p0.address.toString()))
            }

            override fun onError(p0: Status?) {
            }
        })

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val myLocationParent = mapFragment.getView()!!.findViewById<View>(Integer.parseInt("1")).getParent()
        v2 = myLocationParent.getParent();
        getAllPlaces(city)
    }

    private fun onInfoWindowClick(p0: Marker) {
        //TODO("Fix coordinates field")
        val coordinates = p0.position.toString()
        val name = p0.title
        val place = allPlaces?.filter { it.name == name }
        if (place?.isNotEmpty()!!) {
            val intent = Intent(applicationContext, ObjectDetails::class.java)
            intent.putExtra("Place", place[0])
            startActivity(intent)
        }
    }

    /*private fun searchViewCode() {
        val searchView = findViewById<MaterialSearchView>(R.id.search_view)
        searchView.setEllipsize(true)
        searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
    }*/


    private fun searchCity() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this)
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: GooglePlayServicesRepairableException) {
            } catch (e: GooglePlayServicesNotAvailableException) {
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                RESULT_OK -> run {
                    val place = PlaceAutocomplete.getPlace(this, data)
                    val latLng = place.latLng
                    Geocoder(this, Locale.getDefault()).run {
                        val result = getFromLocation(latLng.latitude, latLng.longitude, 1)
                        if (result.size > 0)
                        navCity.title = "${resources.getString(R.string.nav_city_button_menu)}: ${result[0].locality}"
                    }
                    Geocoder(this, Locale.ENGLISH).run {
                        Log.e("s", Locale.ENGLISH.toString())
                        val result = getFromLocation(latLng.latitude, latLng.longitude, 1)
                        if (result.size > 0)
                            city = result[0].locality
                    }
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))

                    val objTypeString = genFilterString("%s, ", checkedItems)
                    val bfString = genFilterString("bf_elements.%s + ", bfElementsDb, checkedBfItems)
                    getAllPlaces(city, objTypeString, bfString[0], bfString[1])
                    getAllPlaces(city)
                }
                PlaceAutocomplete.RESULT_ERROR -> {
                    val status = PlaceAutocomplete.getStatus(this, data)
                    Toast.makeText(applicationContext, status.toString(), Toast.LENGTH_LONG).show()
                }
                RESULT_CANCELED -> {
                }
            }
        }
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorDrawableResourceId: Int, backgroundId: Int): BitmapDescriptor {

        val background = ContextCompat.getDrawable(context, backgroundId)!!
        background.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable (context, vectorDrawableResourceId)!!
        vectorDrawable.setBounds(20, 10, vectorDrawable.intrinsicWidth + 15, vectorDrawable.intrinsicHeight + 10)
        val bitmap = Bitmap.createBitmap (background.intrinsicWidth, background.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap!!.isMyLocationEnabled = true
        } else {
            // Show rationale and request permission.
        }
        val ufa = LatLng(54.7388, 55.9721)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(ufa, 12f))
        mMap!!.setOnMapLongClickListener { latLng -> goToAddingForm(latLng) }
        mMap!!.setInfoWindowAdapter(InfoWindowAdapter(this))
        mMap!!.setOnInfoWindowClickListener {marker -> onInfoWindowClick(marker)}
        mMap!!.setOnMyLocationClickListener { location -> onMyLocationClick(location)  }
        getData()
    }
    private fun onMyLocationClick(location: Location) {
        Geocoder(this, Locale.getDefault()).run {
            val result = getFromLocation(location.latitude, location.longitude, 1)
            if (result.size > 0)
            navCity.title = "${resources.getString(R.string.nav_city_button_menu)}: ${result[0].locality}"
        }

        Geocoder(this, Locale.ENGLISH).run {
            Log.e("s", Locale.ENGLISH.toString())
            val result = getFromLocation(location.latitude, location.longitude, 1)
            if (result.size > 0)
                city = result[0].locality
        }
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 12f))

        val objTypeString = genFilterString("%s, ", checkedItems)
        val bfString = genFilterString("bf_elements.%s + ", bfElementsDb, checkedBfItems)
        getAllPlaces(city, objTypeString, bfString[0], bfString[1])
        getAllPlaces(city)
    }

    private fun goToObjects() {
        if (allPlaces.isEmpty()) {
            Toast.makeText(applicationContext, R.string.empy_list, Toast.LENGTH_LONG).show()
            return
        }
        Intent(applicationContext, ObjectsActivity::class.java).run {

            putExtra("ObjectsActivity", allPlaces)
            putExtra("ObjTypes", objectTypes)
            startActivity(this)
        }
    }

    private fun goToAbout() {
        Intent(applicationContext, About::class.java).run {
            startActivity(this)
        }
    }

    private fun goToChart() {

        if (this::objCity.isInitialized && objCity.isNotEmpty()) {

            Intent(applicationContext, ChartsActivity::class.java).run {
                putExtra("obj", objCity)
                putExtra("objTypes", objectTypes)
                startActivity(this)
            }
            return
        }
        Toast.makeText(applicationContext, R.string.empy_list, Toast.LENGTH_LONG).show()
    }

    private fun goToAddingForm(latLng: LatLng) {
        val addingForm = Intent(applicationContext, NewPlaceActivity::class.java)
        addingForm.putExtra("LatLong", latLng.toString())
        startActivity(addingForm)
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun showViewPointDialog() {
        AlertDialog.Builder(this).run {
            setTitle(R.string.view_point_dialog)
            setSingleChoiceItems(viewPoint, selectedViewPoint) { dialog, which ->  getSelectedViewPointItem(dialog, which)}
            show()
        }
    }

    private fun getSelectedViewPointItem(dialog: DialogInterface, which: Int) {
        selectedViewPoint = which
        setMarks(allPlaces!!)
        dialog.dismiss()
    }

    private fun getViewPointColor(i: Int, obj: Object): Int {
        return when (i) {
            0 -> defineMarkerColor(obj.visual_impairment)
            1 -> defineMarkerColor(obj.physical_disability)
            2 -> defineMarkerColor(obj.mental_issues)
            else -> defineMarkerColor(obj.deafness)
        }
    }

    private fun defineMarkerColor(rate: Int): Int {
        return when {
            rate <= 2 -> R.drawable.ic_red_marker
            rate <= 4 -> R.drawable.ic_yellow_marker
            else -> R.drawable.ic_green_marker
        }
    }

    private fun defineMarkerImage(int: Int): Int {
        return when (int) {
            0 -> {R.drawable.ic_health}
            1 -> {R.drawable.ic_education}
            2 -> {R.drawable.ic_social_protection}
            3 -> {R.drawable.ic_sport}
            4 -> {R.drawable.ic_travel}
            5 -> {R.drawable.ic_infrastructure}
            6 -> {R.drawable.ic_entertainment}
            7 -> {R.drawable.ic_office_buildings}
            else -> {R.drawable.ic_other}
        }
    }


    private fun showBfElementsDialog() {
        flag = checkedBfItems.clone()
        AlertDialog.Builder(this).run {
            setTitle(R.string.bf_filter_dialog)
            setMultiChoiceItems(bfElements, checkedBfItems){ dialog, which, isChecked -> getSelectedBfItems(dialog, which, isChecked) }
            setPositiveButton(R.string.dialog_ok) { dialog, which -> onBfDialogOkClick() }
            setNegativeButton(R.string.dialog_cancel) { dialog, which -> }
            show()
        }
    }

    private fun getSelectedBfItems(dialog: DialogInterface, which: Int, isChecked: Boolean) {
        if (isChecked) {
            checkedBfItems[which] = true
        } else if (checkedBfItems[which]) {
            checkedBfItems[which] = false
        }
    }

    private fun onBfDialogOkClick() {
        if (flag.contentEquals(checkedBfItems))
            return
        getData()
    }

    private fun showObjTypesDialog() {
        flag = checkedItems.clone()
        AlertDialog.Builder(this).run {
            setTitle(R.string.filter_dialog)
            setMultiChoiceItems(objectTypes, checkedItems) { dialog, which, isChecked -> getSelectedItems(dialog, which, isChecked) }
            setPositiveButton(R.string.dialog_ok){ dialog, which -> onObjTypeDialogOkClick() }
            setNegativeButton(R.string.dialog_cancel) {dialog, which ->  }
            show()
        }
    }

    private fun getSelectedItems(dialogInterface: DialogInterface, which: Int, isChecked: Boolean) {
        if (isChecked) {
            checkedItems[which] = true
        } else if (checkedItems[which]) {
            checkedItems[which] = false
        }
    }

    private fun onObjTypeDialogOkClick() {
        if (flag.contentEquals(checkedItems))
            return
        getData()
    }

    private fun genFilterString(format: String, fields: Array<String>, sVal: BooleanArray): Array<String>{

        var str = ""
        if (sVal.contains(true)) {
            for (v in sVal.indices) {
                if (sVal[v])
                    str += String.format(format, fields[v])
            }

            val lastIndx = str.length - 3

            return arrayOf(str.substring(0..lastIndx), sVal.count { it }.toString())
        }
        return arrayOf(str, "")
    }

    private fun genFilterString(format: String, sVal: BooleanArray): String {

        var str = ""

        if (sVal.contains(true)) {
            for (v in sVal.indices) {
                if (sVal[v])
                    str += String.format(format, v)
            }

            val lastIndx = str.length - 3

            str = str.substring(0..lastIndx)
        }
        return str
    }

    private fun setMarks(places: ArrayList<Object>) {
        mMap!!.clear()
        allPlaces = places
        if (places.isEmpty())
            return
        for (pl in places) {
            val name = pl.name
            val address = pl.address
            val coordinates = pl.coordinates
            val comment = pl.comment
            val objType = objectTypes[pl.place_type]
            val issueMark = pl.issue_mark
            val addressTitle = getString(R.string.textEdit_address)
            val commentTitle = getString(R.string.textEdit_comment)
            val objTypeTitle = getString(R.string.textEdit_place_type)
            val markerColor = getViewPointColor(selectedViewPoint, pl)
            val coords = coordinates.substring(10, coordinates.indexOf(')')).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val latLng = LatLng(java.lang.Float.parseFloat(coords[0]).toDouble(), java.lang.Float.parseFloat(coords[1]).toDouble())
            mMap!!.addMarker(MarkerOptions()
                    .position(latLng)
                    .title(name)
                    .snippet(" $objTypeTitle:\t $objType\n $addressTitle:\t $address\n $commentTitle:\t $comment")
                    .icon(bitmapDescriptorFromVector(this, defineMarkerImage(pl.place_type), markerColor)))
        }
    }

    private fun getData() {
        val objTypeString = genFilterString("%s, ", checkedItems)
        val bfString = genFilterString("bf_elements.%s + ", bfElementsDb, checkedBfItems)
        getAllPlaces(city, objTypeString, bfString[0], bfString[1])
    }

    private fun getAllPlaces(city: String, objTypes: String = "", bfElements: String = "", bfVal: String = "" ) {

        val progressBar = findViewById<ProgressBar>(R.id.progressbar)
        progressBar.visibility = View.VISIBLE

        val service = API.create()

        val call = service.getFilteredPlace(city, objTypes, bfElements, bfVal)
        //val call = service.allPlaces
        call.enqueue(object : Callback<ArrayList<Object>> {
            override fun onResponse(call: Call<ArrayList<Object>>, response: Response<ArrayList<Object>>) {
                progressBar.visibility = View.GONE
                val places = response.body()
                //Toast.makeText(applicationContext, "${response.body().toString()}", Toast.LENGTH_LONG).show()
                if (places != null && places.toString() != "[]") {
                    Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()
                    setMarks(response.body()!!)
                } else {
                    Toast.makeText(applicationContext, R.string.no_data_found, Toast.LENGTH_LONG).show()
                    mMap?.clear()!!
                    allPlaces.clear()
                }
            }

            override fun onFailure(call: Call<ArrayList<Object>>, t: Throwable) {
                progressBar.visibility = View.GONE
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun getAllPlaces(city: String) {

        val progressBar = findViewById<ProgressBar>(R.id.progressbar)
        progressBar.visibility = View.VISIBLE

        val service = API.create()

        val call = service.getFilteredPlace(city, "", "", "")
        call.enqueue(object : Callback<ArrayList<Object>> {
            override fun onResponse(call: Call<ArrayList<Object>>, response: Response<ArrayList<Object>>) {
                progressBar.visibility = View.GONE
                val places = response.body()
                //Toast.makeText(applicationContext, "${response.body().toString()}", Toast.LENGTH_LONG).show()
                if (places != null && places.toString() != "[]") {
                    objCity = response.body()!!
                }
            }

            override fun onFailure(call: Call<ArrayList<Object>>, t: Throwable) {
                progressBar.visibility = View.GONE
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return when (id) {
            R.id.obj_type_filter -> {
                showObjTypesDialog()
                true
            }
            R.id.bf_filter -> {
                showBfElementsDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        when (id) {
            R.id.nav_udpate -> getData()
            R.id.nav_objects -> goToObjects()
            R.id.nav_about -> goToAbout()
            R.id.nav_city -> searchCity()
            R.id.nav_statistics -> goToChart()
            R.id.nav_view_point -> showViewPointDialog()
            R.id.nav_help -> {

            }
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}
