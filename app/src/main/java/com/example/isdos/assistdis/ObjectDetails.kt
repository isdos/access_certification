package com.example.isdos.assistdis

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.CheckBox
import kotlinx.android.synthetic.main.activity_object_details.*
import kotlinx.android.synthetic.main.content_object_details.*

class ObjectDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_object_details)
        setSupportActionBar(toolbar)

        val objTypes = resources.getStringArray(R.array.obj_types)
        val place = intent.getParcelableExtra<Object>("Place")!!

        titleTextView.text = "${getString(R.string.textEdit_title)}: ${place.name}"
        cityTextView.text = String.format("%s: %s",getString(R.string.textEdit_city), place.city)
        addressTextView.text = "${getString(R.string.textEdit_address)}: ${place.address}"
        objTypeTextView.text = "${getString(R.string.textEdit_place_type)}: ${objTypes[place.place_type]}"
        commentTextView.text = "${getString(R.string.textEdit_comment)}: \n${place.comment}"

        visualDetails.rating = place.visual_impairment.toFloat()
        mentalDetails.rating = place.mental_issues.toFloat()
        physicalDetails.rating = place.physical_disability.toFloat()
        hearingDetails.rating = place.deafness.toFloat()

        isChecked(emergencyEvacuationCheck, place.bf_elements.emergency_evacuation)
        isChecked(parkingCheck, place.bf_elements.parking)
        isChecked(routesObjCheck, place.bf_elements.routes)
        isChecked(entranceCheck,place.bf_elements.entrance)
        isChecked(accessBlindCheck, place.bf_elements.access_blind)
        isChecked(accessDeafCheck, place.bf_elements.access_deaf)
        isChecked(infoSignCheck, place.bf_elements.info_signs)
        isChecked(trainedStaffCheck, place.bf_elements.trained_staff)
        isChecked(liftsStairsCheck, place.bf_elements.lifts_stairs)

    }

    private fun isChecked(checkBox: CheckBox,state: Int) {
        if(state == 0)
            checkBox.visibility = View.GONE
    }
}
