package com.example.isdos.assistdis

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.example.isdos.assistdis.ObjectFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_object.view.*


class ObjectAdapter(
        private val mValues: List<Object>,
        private val objTypes: Array<String>,
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<ObjectAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Object

            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_object, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mIdView.text = holder.mIdView.getText().toString() + item.getId
        holder.mContentView.text = holder.mContentView.getText().toString() + ": \n" + item.comment
        holder.mTitleView.text = holder.mTitleView.getText().toString() + ": \n" + item.name
        holder.mAddressView.text = holder.mAddressView.getText().toString() + ": \n" + item.address
        holder.mObjTypeView.text = holder.mObjTypeView.getText().toString() + ": \n" + objTypes[item.place_type]

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mContentView: TextView = mView.content
        val mTitleView: TextView = mView.titleView
        val mAddressView: TextView = mView.addressView
        val mObjTypeView: TextView = mView.objTypeView

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
